package com.carontime.serwerconfigrepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SerwerconfigrepoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SerwerconfigrepoApplication.class, args);
    }

}

